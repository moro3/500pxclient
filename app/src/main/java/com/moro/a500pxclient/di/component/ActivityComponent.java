package com.moro.a500pxclient.di.component;

import com.moro.a500pxclient.activity.BaseActivity;
import com.moro.a500pxclient.di.module.PresentersModule;
import com.moro.a500pxclient.di.scope.ActivityScope;
import com.moro.a500pxclient.view.PhotoGridView;
import dagger.Subcomponent;

/**
 * Created with Android Studio.
 * User: Vitalii Morozov
 * Date: 29.02.16
 * Time: 17:06
 */
@ActivityScope
@Subcomponent(modules = { PresentersModule.class })
public interface ActivityComponent {
  void inject(BaseActivity activity);

  void inject(PhotoGridView view);
}
