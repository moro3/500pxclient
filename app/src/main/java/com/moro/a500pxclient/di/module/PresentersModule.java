package com.moro.a500pxclient.di.module;

import com.moro.a500pxclient.Api500px;
import com.moro.a500pxclient.di.scope.ActivityScope;
import com.moro.a500pxclient.presenter.PhotoGridPresenter;
import dagger.Module;
import dagger.Provides;


/**
 * Created with Android Studio.
 * User: Vitalii Morozov
 * Date: 29.02.16
 * Time: 21:43
 */
@Module
public class PresentersModule {

  @Provides @ActivityScope public PhotoGridPresenter providePhotoGridPresenter (Api500px api) {
    return new PhotoGridPresenter(api);
  }
}
