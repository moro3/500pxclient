package com.moro.a500pxclient.di.component;

import android.app.Application;
import com.moro.a500pxclient.di.module.AppModule;
import dagger.Component;
import javax.inject.Singleton;

/**
 * Created with Android Studio.
 * User: Vitalii Morozov
 * Date: 29.02.16
 * Time: 15:26
 */
@Singleton
@Component(modules = { AppModule.class })
public interface AppComponent {
  Application application();

  ActivityComponent plus();
}
