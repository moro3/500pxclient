package com.moro.a500pxclient.di.scope;

import javax.inject.Scope;

/**
 * Created with Android Studio.
 * User: Vitalii Morozov
 * Date: 29.02.16
 * Time: 17:06
 */
@Scope
public @interface ActivityScope {}
