package com.moro.a500pxclient.di.module;

import android.app.Application;
import com.moro.a500pxclient.Api500px;
import com.moro.a500pxclient.BuildConfig;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created with Android Studio.
 * User: Vitalii Morozov
 * Date: 29.02.16
 * Time: 15:33
 */
@Module
public class AppModule {
  private Application application;

  public AppModule(final Application application) {
    this.application = application;
  }

  @Provides @Singleton public Application provideApplication() {
    return application;
  }

  @Provides @Singleton public Api500px provideApi(OkHttpClient client) {
    return retrofit(client).create(Api500px.class);
  }

  private Retrofit retrofit(OkHttpClient client) {
    return new Retrofit.Builder().baseUrl(Api500px.BASE_URL)
        .addConverterFactory(JacksonConverterFactory.create())
        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
        .client(client)
        .build();
  }

  @Provides @Singleton public OkHttpClient provideOkHttpClient() {
    OkHttpClient.Builder builder = new OkHttpClient.Builder();
    if (BuildConfig.DEBUG) {
      HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
      loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
      builder.addInterceptor(loggingInterceptor);
    }
    return builder.build();
  }
}
