package com.moro.a500pxclient;

import com.moro.a500pxclient.pojo.PhotosListResponse;
import com.moro.a500pxclient.util.AppConstants;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Vitalii Morozov on 11.03.16, 12:31.
 */
public interface Api500px {

  String BASE_URL = "https://api.500px.com/v1/";

  @GET("photos?feature=popular&image_size="
      + AppConstants.SMALL_IMAGE_SIZE
      + ","
      + AppConstants.LARGE_IMAGE_SIZE
      + "&consumer_key=wB4ozJxTijCwNuggJvPGtBGCRqaZVcF6jsrzUadF")
  Observable<PhotosListResponse> getPhotos(@Query("page") int pageNum);
}

