package com.moro.a500pxclient.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.parceler.Parcel;

/**
 * Created by Vitalii Morozov on 11.03.16, 13:18.
 */
@Parcel
@JsonIgnoreProperties(ignoreUnknown = true)
public class Image {
  private int size;
  private String url;
  private String format;

  public String getUrl() {
    return url;
  }

  public void setUrl(final String url) {
    this.url = url;
  }

  public String getFormat() {
    return format;
  }

  public void setFormat(final String format) {
    this.format = format;
  }

  public int getSize() {
    return size;
  }

  public void setSize(final int size) {
    this.size = size;
  }
}
