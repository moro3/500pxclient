package com.moro.a500pxclient.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.parceler.Parcel;

/**
 * Created by Vitalii Morozov on 11.03.16, 15:53.
 */
@Parcel
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {
  private String username;

  public String getUsername() {
    return username;
  }

  public void setUsername(final String username) {
    this.username = username;
  }
}
