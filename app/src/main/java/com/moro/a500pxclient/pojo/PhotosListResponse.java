package com.moro.a500pxclient.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * Created by Vitalii Morozov on 11.03.16, 13:12.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PhotosListResponse {

  private String feature;
  @JsonProperty("current_page") private long currentPage;
  @JsonProperty("total_pages") private long totalPages;
  @JsonProperty("total_items") private long totalItems;
  private List<Photo> photos;

  public String getFeature() {
    return feature;
  }

  public void setFeature(final String feature) {
    this.feature = feature;
  }

  public long getCurrentPage() {
    return currentPage;
  }

  public void setCurrentPage(final long currentPage) {
    this.currentPage = currentPage;
  }

  public long getTotalPages() {
    return totalPages;
  }

  public void setTotalPages(final long totalPages) {
    this.totalPages = totalPages;
  }

  public long getTotalItems() {
    return totalItems;
  }

  public void setTotalItems(final long totalItems) {
    this.totalItems = totalItems;
  }

  public List<Photo> getPhotos() {
    return photos;
  }

  public void setPhotos(final List<Photo> photos) {
    this.photos = photos;
  }
}
