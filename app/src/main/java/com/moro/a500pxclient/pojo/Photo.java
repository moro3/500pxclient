package com.moro.a500pxclient.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.moro.a500pxclient.util.AppConstants;
import java.util.List;
import org.parceler.Parcel;

/**
 * Created by Vitalii Morozov on 11.03.16, 13:13.
 */
@Parcel
@JsonIgnoreProperties(ignoreUnknown = true)
public class Photo {

  private long id;
  private String name;
  private String camera;
  private User user;
  private List<Image> images;

  public long getId() {
    return id;
  }

  public void setId(final long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public List<Image> getImages() {
    return images;
  }

  public Image getSmallImage() {
    return getImage(AppConstants.SMALL_IMAGE_SIZE);
  }

  public Image getLargeImage() {
    return getImage(AppConstants.LARGE_IMAGE_SIZE);
  }

  public Image getImage(int imageSize) {
    Image image = null;
    for (Image i : images) {
      if (i.getSize() == imageSize) {
        image = i;
      }
    }
    if (image == null) {
      throw new IllegalStateException("We didn't get image of size " + imageSize + ", check your request!");
    }
    return image;
  }

  public void setImages(final List<Image> images) {
    this.images = images;
  }

  public String getCamera() {
    return camera;
  }

  public void setCamera(final String camera) {
    this.camera = camera;
  }

  public User getUser() {
    return user;
  }

  public void setUser(final User user) {
    this.user = user;
  }
}
