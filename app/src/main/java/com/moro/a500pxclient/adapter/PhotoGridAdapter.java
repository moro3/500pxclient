package com.moro.a500pxclient.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.moro.a500pxclient.R;
import com.moro.a500pxclient.activity.FullscreenGalleryActivity;
import com.moro.a500pxclient.pojo.Photo;
import com.squareup.picasso.Picasso;
import java.util.List;

/**
 * Created by Vitalii Morozov on 11.03.16, 13:06.
 */
public class PhotoGridAdapter extends RecyclerView.Adapter<PhotoGridAdapter.PhotoGridViewHolder> {

  private List<Photo> photos;

  @Override public PhotoGridViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
    return new PhotoGridViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid, parent, false));
  }

  @Override public void onBindViewHolder(final PhotoGridViewHolder holder, final int position) {
    holder.bind(getItem(position));
  }

  @Override public int getItemCount() {
    return photos == null ? 0 : photos.size();
  }

  public Photo getItem(int position) {
    return photos.get(position);
  }

  public void appendPhotos(List<Photo> photos) {
    if (this.photos != null) {
      int size = getItemCount();
      this.photos.addAll(photos);
      notifyItemRangeInserted(size, photos.size());
    } else {
      this.photos = photos;
      notifyDataSetChanged();
    }
  }

  class PhotoGridViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.image) ImageView image;

    public PhotoGridViewHolder(final View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }

    public void bind(Photo photo) {
      Picasso.with(itemView.getContext()).load(photo.getSmallImage().getUrl()).into(image);
    }

    @OnClick(R.id.root) public void onItemClick() {
      FullscreenGalleryActivity.showGallery(itemView.getContext(), photos, getAdapterPosition());
    }
  }
}
