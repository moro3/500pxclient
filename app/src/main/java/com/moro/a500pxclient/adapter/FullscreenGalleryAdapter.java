package com.moro.a500pxclient.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import com.moro.a500pxclient.pojo.Photo;
import com.moro.a500pxclient.widget.FullscreenGalleryItemView;
import java.util.List;

/**
 * Created by Vitalii Morozov on 11.03.16, 16:20.
 */
public class FullscreenGalleryAdapter extends PagerAdapter {

  List<Photo> photos;

  @Override public int getCount() {
    return photos == null ? 0 : photos.size();
  }

  @Override public boolean isViewFromObject(final View view, final Object object) {
    return view == object;
  }

  @Override public Object instantiateItem(final ViewGroup container, final int position) {
    FullscreenGalleryItemView view =
        new FullscreenGalleryItemView(container.getContext()).initWithPhoto(getItem(position));
    container.addView(view);
    return view;
  }

  @Override public void destroyItem(final ViewGroup container, final int position, final Object object) {
    container.removeView((View) object);
  }

  public Photo getItem(int position) {
    return photos.get(position);
  }

  public void refreshGallery(List<Photo> photos) {
    if (this.photos != null) {
      this.photos.clear();
    }
    this.photos = photos;
    notifyDataSetChanged();
  }
}
