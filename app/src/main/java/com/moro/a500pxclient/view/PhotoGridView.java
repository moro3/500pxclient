package com.moro.a500pxclient.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import com.moro.a500pxclient.activity.PhotoGridActivity;
import com.moro.a500pxclient.adapter.PhotoGridAdapter;
import com.moro.a500pxclient.pojo.Photo;
import com.moro.a500pxclient.presenter.PhotoGridPresenter;
import com.moro.a500pxclient.util.AppConstants;
import java.util.List;
import javax.inject.Inject;

/**
 * Created by Vitalii Morozov on 11.03.16, 12:29.
 */
public class PhotoGridView extends RecyclerView {

  @Inject PhotoGridPresenter presenter;
  private PhotoGridAdapter adapter;
  private LoadMoreRecyclerViewScrollListener infiniteScrollListener;

  public PhotoGridView(final Context context) {
    this(context, null);
  }

  public PhotoGridView(final Context context, @Nullable final AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public PhotoGridView(final Context context, @Nullable final AttributeSet attrs, final int defStyle) {
    super(context, attrs, defStyle);
    ((PhotoGridActivity) context).getActivityComponent().inject(this);
    GridLayoutManager layoutManager = new GridLayoutManager(context, AppConstants.GRID_COLUMNS, LinearLayoutManager.VERTICAL, false);
    setLayoutManager(layoutManager);
    adapter = new PhotoGridAdapter();
    setAdapter(adapter);
    infiniteScrollListener = new LoadMoreRecyclerViewScrollListener(layoutManager) {
      @Override public boolean onLoadMoreBackward() {
        presenter.loadImages();
        return true;
      }

      @Override public boolean onLoadMoreForward() {
        // Nothing to do, we start from the top
        return false;
      }
    };
    addOnScrollListener(infiniteScrollListener);
  }

  @Override protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    presenter.takeView(this);
  }

  @Override protected void onDetachedFromWindow() {
    presenter.dropView();
    super.onDetachedFromWindow();
  }

  public void appendPhotos(List<Photo> photos) {
    infiniteScrollListener.setIsLoadingBackward(false);
    adapter.appendPhotos(photos);
  }
}

abstract class LoadMoreRecyclerViewScrollListener extends RecyclerView.OnScrollListener {

  private boolean isLoadingBackward; // True if we are still waiting for the last set of data to load.
  private boolean isLoadingForward; // True if we are still waiting for the last set of data to load.
  private int visibleThreshold = 4; // The minimum amount of items to have below your current scroll position before loading more.
  int firstVisibleItem, visibleItemCount, totalItemCount;

  private LinearLayoutManager layoutManager;

  public LoadMoreRecyclerViewScrollListener(LinearLayoutManager layoutManager) {
    this.layoutManager = layoutManager;
  }

  @Override
  public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
    visibleItemCount = recyclerView.getChildCount();
    totalItemCount = layoutManager.getItemCount();
    firstVisibleItem = layoutManager.findFirstVisibleItemPosition();

    // Load data only if we are at one of the edges of the list and moving towards it
    // Direction of the scroll depends on layout direction
    if (!isLoadingBackward && (totalItemCount - visibleItemCount <= firstVisibleItem + visibleThreshold)) {
      if (dy < 0 && layoutManager.getReverseLayout() || dy > 0 && !layoutManager.getReverseLayout()) {
        // Scrolling up and start of the list is at the bottom or scrolling down and start is at the top
        if (onLoadMoreBackward()) {
          setIsLoadingBackward(true);
        }
      }
    } else if (!isLoadingForward && firstVisibleItem <= visibleThreshold) {
      if (dy > 0 && layoutManager.getReverseLayout() || dy < 0 && !layoutManager.getReverseLayout()) {
        // Scrolling down and start of the list is at the bottom or scrolling up and start is at the top
        if (onLoadMoreForward()) {
          setIsLoadingForward(true);
        }
      }
    }
  }


  public void setIsLoadingBackward(boolean isLoadingBackward) {
    this.isLoadingBackward = isLoadingBackward;
  }

  public void setIsLoadingForward(boolean isLoadingForward) {
    this.isLoadingForward = isLoadingForward;
  }

  /**
   * Backward here is in terms of time the content was posted to timelime (feed) or message list. In chat going backwards means scrolling up,
   * in feed it's scrolling down
   *
   * @return true if started loading, false if ignored
   */
  public abstract boolean onLoadMoreBackward();

  /**
   * Forward here is in terms of time the content was posted to timelime (feed) or message list. In chat going backwards means scrolling up,
   * in feed it's scrolling down
   *
   * @return true if started loading, false if ignored
   */
  public abstract boolean onLoadMoreForward();
}
