package com.moro.a500pxclient.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import com.moro.a500pxclient.adapter.FullscreenGalleryAdapter;
import com.moro.a500pxclient.pojo.Photo;
import com.moro.a500pxclient.presenter.FullscreenGalleryPresenter;
import java.util.List;

/**
 * Created by Vitalii Morozov on 11.03.16, 15:32.
 */
public class FullscreenGalleryView extends ViewPager {

  private FullscreenGalleryPresenter presenter;
  private FullscreenGalleryAdapter adapter;

  public FullscreenGalleryView(final Context context, FullscreenGalleryPresenter presenter) {
    this(context);
    this.presenter = presenter;
  }

  public FullscreenGalleryView(final Context context) {
    this(context, ((AttributeSet) null));
  }

  public FullscreenGalleryView(final Context context, final AttributeSet attrs) {
    super(context, attrs);
    adapter = new FullscreenGalleryAdapter();
    setAdapter(adapter);
  }

  @Override protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    presenter.takeView(this);
  }

  @Override protected void onDetachedFromWindow() {
    presenter.dropView();
    super.onDetachedFromWindow();
  }

  public void showPhoto(List<Photo> photos, int indexOfPhotoToShow) {
    adapter.refreshGallery(photos);
    setCurrentItem(indexOfPhotoToShow, false);
  }

  public void setPresenter(final FullscreenGalleryPresenter presenter) {
    this.presenter = presenter;
  }
}
