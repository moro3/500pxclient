package com.moro.a500pxclient.util;

/**
 * Created by Vitalii Morozov on 11.03.16, 17:23.
 */
public class AppConstants {

  public static final int GRID_COLUMNS = 2;
  public static final int SMALL_IMAGE_SIZE = 3;
  public static final int LARGE_IMAGE_SIZE = 1080;

  private AppConstants() {}



}
