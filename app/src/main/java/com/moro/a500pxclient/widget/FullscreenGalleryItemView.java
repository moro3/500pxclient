package com.moro.a500pxclient.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.moro.a500pxclient.R;
import com.moro.a500pxclient.pojo.Photo;
import com.squareup.picasso.Picasso;

/**
 * Created by Vitalii Morozov on 11.03.16, 16:44.
 */
public class FullscreenGalleryItemView extends RelativeLayout {

  @Bind(R.id.image) ImageView image;
  @Bind(R.id.overlay) LinearLayout overlayContainer;
  @Bind(R.id.photo_name) TextView photoName;
  @Bind(R.id.author_name) TextView authorName;
  @Bind(R.id.camera_model) TextView cameraModel;

  public FullscreenGalleryItemView(final Context context) {
    this(context, null);
  }

  public FullscreenGalleryItemView(final Context context, final AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public FullscreenGalleryItemView(final Context context, final AttributeSet attrs, final int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    LayoutInflater.from(context).inflate(R.layout.item_fullscreen_gallery, this);
    ButterKnife.bind(this);
  }

  public FullscreenGalleryItemView initWithPhoto(Photo photo) {
    Picasso.with(getContext()).load(photo.getLargeImage().getUrl()).into(image);
    photoName.setText(photo.getName());
    authorName.setText(photo.getUser().getUsername());
    cameraModel.setText(photo.getCamera());
    return this;
  }
}
