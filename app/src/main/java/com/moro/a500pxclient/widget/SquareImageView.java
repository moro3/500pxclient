package com.moro.a500pxclient.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Vitalii Morozov on 11.03.16, 14:33.
 */
public class SquareImageView extends ImageView {
  public SquareImageView(final Context context) {
    this(context, null);
  }

  public SquareImageView(final Context context, final AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public SquareImageView(final Context context, final AttributeSet attrs, final int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @SuppressWarnings("SuspiciousNameCombination") @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    int width = getMeasuredWidth();
    int height = getMeasuredHeight();

    // Optimization so we don't measure twice unless we need to
    if (width != height) {
      setMeasuredDimension(width, width);
    }
  }
}

