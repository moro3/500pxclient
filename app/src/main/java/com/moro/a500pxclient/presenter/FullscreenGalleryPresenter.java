package com.moro.a500pxclient.presenter;

import com.moro.a500pxclient.pojo.Photo;
import com.moro.a500pxclient.view.FullscreenGalleryView;
import java.util.List;

/**
 * Created by Vitalii Morozov on 11.03.16, 15:33.
 */
public class FullscreenGalleryPresenter extends ViewPresenter<FullscreenGalleryView> {
  private final List<Photo> photos;
  private int indexOfPhotoToShow;

  public FullscreenGalleryPresenter(final List<Photo> photos, int indexOfPhotoToShow) {
    this.photos = photos;
    this.indexOfPhotoToShow = indexOfPhotoToShow;
  }

  @Override public void takeView(final FullscreenGalleryView view) {
    super.takeView(view);
    getView().showPhoto(photos, indexOfPhotoToShow);
  }
}
