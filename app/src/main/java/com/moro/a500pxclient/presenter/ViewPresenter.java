package com.moro.a500pxclient.presenter;

import android.support.annotation.CallSuper;

/**
 * Created by Vitalii Morozov on 11.03.16, 13:27.
 */
abstract class ViewPresenter<V> {
  private V view;

  @CallSuper public void takeView(V view) {
    this.view = view;
  }

  @CallSuper public void dropView() {
    view = null;
  }

  public V getView() {
    return view;
  }
}
