package com.moro.a500pxclient.presenter;

import com.moro.a500pxclient.Api500px;
import com.moro.a500pxclient.pojo.PhotosListResponse;
import com.moro.a500pxclient.util.RxUtils;
import com.moro.a500pxclient.view.PhotoGridView;
import timber.log.Timber;

/**
 * Created by Vitalii Morozov on 11.03.16, 12:30.
 */
public class PhotoGridPresenter extends ViewPresenter<PhotoGridView> {

  private final Api500px api;
  private int currentPage = 0;

  public PhotoGridPresenter(final Api500px api) {
    this.api = api;
  }

  @Override public void takeView(final PhotoGridView view) {
    super.takeView(view);
    loadImages();
  }

  public void loadImages() {
    loadImagesFromPage(++currentPage);
  }

  private void loadImagesFromPage(int pageNum) {
    api.getPhotos(pageNum).compose(RxUtils.<PhotosListResponse>applySchedulers())
    .subscribe(
        photosList -> getView().appendPhotos(photosList.getPhotos()),
        error -> Timber.e(error, "Error while getting photos page " + pageNum),
        () -> Timber.d("Done loading page " + pageNum)
    );
  }
}
