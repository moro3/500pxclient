package com.moro.a500pxclient;

import android.app.Application;
import com.moro.a500pxclient.di.component.AppComponent;
import com.moro.a500pxclient.di.component.DaggerAppComponent;
import com.moro.a500pxclient.di.module.AppModule;
import timber.log.Timber;

/**
 * Created by Vitalii Morozov on 11.03.16, 12:41.
 */
public class Application500px extends Application {

  private AppComponent appComponent;

  @Override public void onCreate() {
    super.onCreate();
    appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    Timber.plant(new Timber.DebugTree());
  }

  public AppComponent getAppComponent() {
    return appComponent;
  }
}
