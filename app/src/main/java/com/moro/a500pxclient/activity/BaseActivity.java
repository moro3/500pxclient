package com.moro.a500pxclient.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.moro.a500pxclient.Application500px;
import com.moro.a500pxclient.di.component.ActivityComponent;

/**
 * Created by Vitalii Morozov on 11.03.16, 15:17.
 */
public class BaseActivity extends AppCompatActivity {
  private ActivityComponent activityComponent;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    activityComponent = ((Application500px) getApplication()).getAppComponent().plus();
    activityComponent.inject(this);
  }

  public ActivityComponent getActivityComponent() {
    return activityComponent;
  }
}
