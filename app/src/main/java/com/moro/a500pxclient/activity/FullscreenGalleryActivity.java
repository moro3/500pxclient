package com.moro.a500pxclient.activity;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ShareCompat;
import android.support.v4.view.GestureDetectorCompat;
import android.view.GestureDetector;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.moro.a500pxclient.R;
import com.moro.a500pxclient.pojo.Photo;
import com.moro.a500pxclient.presenter.FullscreenGalleryPresenter;
import com.moro.a500pxclient.view.FullscreenGalleryView;
import java.util.List;
import org.parceler.Parcels;

/**
 * Created by Vitalii Morozov on 11.03.16, 15:16.
 */
public class FullscreenGalleryActivity extends BaseActivity {

  private static final String EXTRA_PHOTOS = "EXTRA_PHOTOS";
  private static final String EXTRA_INDEX_OF_PHOTO_TO_SHOW = "EXTRA_INDEX_OF_PHOTO_TO_SHOW";

  private static final String KEY_INDEX_OF_PHOTO_TO_SHOW = "KEY_INDEX_OF_PHOTO_TO_SHOW";

  private boolean isFullScreen;
  @Bind(R.id.gallery_pager_view) FullscreenGalleryView galleryView;
  @Bind(R.id.button_share) FloatingActionButton shareButton;
  private List<Photo> photos;
  private ObjectAnimator fabAnimator;

  public static void showGallery(Context context, List<Photo> photos, int indexOfPhotoToShow) {
    Intent intent = new Intent(context, FullscreenGalleryActivity.class);
    intent.putExtra(EXTRA_PHOTOS, Parcels.wrap(photos));
    intent.putExtra(EXTRA_INDEX_OF_PHOTO_TO_SHOW, indexOfPhotoToShow);
    context.startActivity(intent);
  }

  @Override protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_fullscreen_gallery);
    ButterKnife.bind(this);
    photos = Parcels.unwrap(getIntent().getParcelableExtra(EXTRA_PHOTOS));
    int indexOfPhotoToShow = getIntent().getIntExtra(EXTRA_INDEX_OF_PHOTO_TO_SHOW, 0);
    if (savedInstanceState != null) {
      indexOfPhotoToShow = savedInstanceState.getInt(KEY_INDEX_OF_PHOTO_TO_SHOW);
    }
    FullscreenGalleryPresenter presenter = new FullscreenGalleryPresenter(photos, indexOfPhotoToShow);
    galleryView.setPresenter(presenter);
    galleryView.setBackgroundColor(getResources().getColor(android.R.color.black));
    GestureDetectorCompat gestureDetector = new GestureDetectorCompat(this, new MyGestureDetectorListener());
    galleryView.setOnTouchListener((v, event) -> gestureDetector.onTouchEvent(event));
    if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
      fabAnimator = ObjectAnimator.ofFloat(shareButton, "translationY", 0, -getNavBarHeight());
    } else {
      fabAnimator = ObjectAnimator.ofFloat(shareButton, "translationX", 0, -getNavBarHeight());
    }
    fabAnimator.setDuration(500);
  }

  @Override protected void onSaveInstanceState(final Bundle outState) {
    outState.putInt(KEY_INDEX_OF_PHOTO_TO_SHOW, galleryView.getCurrentItem());
    super.onSaveInstanceState(outState);
  }

  @Override public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);
    if (hasFocus) {
      enableFullscreen();
    }
  }

  public void onShareButtonClick(View v) {
    Photo currentPhoto = photos.get(galleryView.getCurrentItem());
    Intent shareIntent = ShareCompat.IntentBuilder.from(this)
        .setType("text/plain")
        .setText(currentPhoto.getLargeImage().getUrl())
        .getIntent();
    if (shareIntent.resolveActivity(getPackageManager()) != null) {
      startActivity(shareIntent);
    }
  }

  private void enableFullscreen() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
      getWindow().getDecorView()
          .setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
              | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
              | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
              | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
              | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
              | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    } else {
      getWindow().getDecorView()
          .setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
              | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
              | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
              | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
              | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
          );
    }
    fabAnimator.reverse();
    isFullScreen = true;
  }

  private void disableFullscreen() {
    getWindow().getDecorView()
        .setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    fabAnimator.start();
    isFullScreen = false;
  }

  private int getNavBarHeight() {
    boolean hasMenuKey = ViewConfiguration.get(this).hasPermanentMenuKey();
    boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
    if (hasMenuKey && hasBackKey) return 0;
    Resources resources = getResources();
    int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
    if (resourceId > 0) {
      return resources.getDimensionPixelSize(resourceId);
    }
    return 0;
  }

  class MyGestureDetectorListener implements GestureDetector.OnGestureListener {

    @Override public boolean onDown(final MotionEvent e) {
      return true;
    }

    @Override public void onShowPress(final MotionEvent e) {

    }

    @Override public boolean onSingleTapUp(final MotionEvent e) {
      if (isFullScreen) {
        disableFullscreen();
      } else {
        enableFullscreen();
      }
      return true;
    }

    @Override
    public boolean onScroll(final MotionEvent e1, final MotionEvent e2, final float distanceX, final float distanceY) {
      return false;
    }

    @Override public void onLongPress(final MotionEvent e) {

    }

    @Override
    public boolean onFling(final MotionEvent e1, final MotionEvent e2, final float velocityX, final float velocityY) {
      return false;
    }
  }
}
