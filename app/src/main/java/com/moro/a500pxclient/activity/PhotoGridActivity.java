package com.moro.a500pxclient.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.moro.a500pxclient.R;

public class PhotoGridActivity extends BaseActivity {
  @Bind(R.id.toolbar) Toolbar toolbar;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_photo_grid);
    ButterKnife.bind(this);
    setSupportActionBar(toolbar);
  }
}
